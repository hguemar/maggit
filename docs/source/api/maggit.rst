maggit package api
==================

.. module:: maggit

.. toctree::
    :maxdepth: 1

    maggit.db

.. autoclass:: maggit.Sha
    :show-inheritance:
    :members:

.. autoclass:: maggit.Repo
    :show-inheritance:
    :members:
    :undoc-members:
    :inherited-members:

.. autoclass:: maggit.Person
    :members:

.. autoclass:: maggit.Entry
    :members:

.. autoclass:: maggit.Blob
    :show-inheritance:

.. autoclass:: maggit.Tree
    :show-inheritance:

.. autoclass:: maggit.Commit
    :show-inheritance:

.. autoclass:: maggit.Tag
    :show-inheritance:

maggit.gitObjects module
-------------------------

.. module:: maggit.gitObjects

.. autoclass:: GitObject

maggit.refs module
------------------

.. automodule:: maggit.refs
    :members:
    :undoc-members:
    :show-inheritance:

Maggit's documentation
======================

Contents:

.. toctree::
   :maxdepth: 1

   installing
   tutorial
   api/maggit.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

from .gitObject import GitObject
from .utils import parse_signature

_setattr = object.__setattr__

class Tag(GitObject):
    """ A tag object.

    Attributes:
        object(:class:`~maggit.gitObjects.GitObject`)      : The git object tagged by this tag.
        tag(str)               : The name of the tag.
        tagger(:class:`~maggit.Person`)          : The person who create the tag.
        tagger_date(timedate)  : When the tag was created.
        first_line(str)        : The first line of the tag message.
        message(str)           : The full tag message (including the first line).
    """
    __slots__ = ('object', '_object', '_objectType',
                 'tag',
                 'tagger', 'tagger_date', '_tagger',
                 'first_line', 'message', '_messageLines')

    def _compute_object(self):
        return self._objectType(self.repo, self._object)

    def _compute_tagger(self):
        try:
            tagger, date = parse_signature(self._tagger)
        except TypeError:
            tagger, date = None, None
        _setattr(self, 'tagger_date', date)
        return tagger

    def _compute_tagger_date(self):
        try:
            tagger, date = parse_signature(self._tagger)
        except TypeError:
            tagger, date = None, None
        _setattr(self, 'tagger', tagger)
        return date

    def _compute_first_line(self):
         return self._messageLines[0].decode('utf8')

    def _compute_message(self):
        return b'\n'.join(self._messageLines).decode('utf8')

    def _read(self):
        (_object, objtype, tag, tagger, messageLines) = self.repo.db.tag_content(self._sha)
        _setattr(self, '_object', _object)
        _setattr(self, '_objectType', byte_to_class(objtype))
        _setattr(self, 'tag', tag)
        _setattr(self, '_tagger', tagger)
        _setattr(self, '_messageLines', messageLines)


from .blob import Blob
from .tree import Tree
from .commit import Commit

# This is no especially specific to Tag
# Ideally I would have defined it in gitObjects.__init__.py but
# this leads to cyclic import.
# So define it here and lets __init__.py import it instead.
_type_byte_to_class = {
    b'blob'  : Blob,
    b'tree'  : Tree,
    b'commit': Commit,
    b'tag'   : Tag
}

byte_to_class = _type_byte_to_class.get

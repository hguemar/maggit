# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit import repo as _repo
import os
from .util import GitWrapper

@pytest.fixture(scope='session', params=[True, False])
def ref_base(tmpdir_factory, request):
    tmpdir = tmpdir_factory.mktemp('git_ref')
    gitwrapper = GitWrapper(tmpdir)
    assert os.environ['MAGGIT_GIT_REF_BASE']
    if request.param:
        gitwrapper('clone', '--bare', os.environ['MAGGIT_GIT_REF_BASE'], '.')
    else:
        gitwrapper('clone', os.environ['MAGGIT_GIT_REF_BASE'], '.')
    return _repo.Repo(gitdir=str(tmpdir)), gitwrapper

def get_sha(brancheName, gitwrapper):
    return gitwrapper('rev-parse', brancheName).decode()


@pytest.mark.skipif('MAGGIT_GIT_REF_BASE' not in os.environ,
                    reason="You need to set MAGGIT_GIT_REF_BASE environment variable")
class TestRealGitBase:
    def test_branches(self, ref_base):
        repo, gitwrapper = ref_base
        #yes, we rebuild all the dict {'branches': 'sha'} from call to git command
        out = gitwrapper('branch', raw=True)
        branches = {}
        for b in out.split(b'\n')[:-1]:
            if b.startswith(b'*'):
                b = b[1:]
            b = b.strip().decode()
            branches[b] = get_sha(b, gitwrapper)
        print(branches)
        assert {n:str(b.sha) for n,b in repo.branches.items()} == branches

    def test_tags(self, ref_base):
        repo, gitwrapper = ref_base
        #yes, we rebuild all the dict {'tags': 'sha'} from call to git command
        out = gitwrapper('tag', raw=True)
        tags = {}
        tags_obj = {}
        for t in out.split(b'\n')[:-1]:
            t = t.strip().decode()
            tags[t] = get_sha(t, gitwrapper)
            sha = get_sha("%s^{}"%t, gitwrapper)
            tags_obj[t] = sha
        assert {n:str(t.sha) for n,t in repo.tags.items()} == tags
        assert {n:str(t.object.sha) for n,t in repo.tags.items()} == tags
        tags_not_commit = []
        for n, t in tags_obj.items():
            if gitwrapper('cat-file', '-t', t) != b'commit':
                tags_not_commit.append(n)
        # Try to read only tags pointing to commit
        assert {n:str(t.commit.sha) for n,t in repo.tags.items() if n not in tags_not_commit} == {n:s for n,s in tags_obj.items() if n not in tags_not_commit}
        # Tags pointing to not commit object should raise
        for n in tags_not_commit:
            with pytest.raises(ValueError):
                repo.tags[n].commit.sha

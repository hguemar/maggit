# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db import io
from binascii import unhexlify, hexlify
import os
import time, zlib
from .util import GitWrapper

def git_create_blob(gitwrapper, content):
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=content)
    return unhexlify(out)

_to_bytes = lambda path, mode_sha : mode_sha[0] + b' ' + path + bytes([0]) + mode_sha[1]
def git_create_tree(gitwrapper, entries):
    content = b''.join(_to_bytes(*entry) for entry in entries)
    out = gitwrapper('hash-object', '-t', 'tree', '-w', '--stdin', input=content)
    return unhexlify(out)

def git_create_commit(gitwrapper, treesha, message, author, authorMail, authorDate, committer, committerMail, committerDate, parents=[]):
    env = dict(os.environ)
    env['GIT_AUTHOR_NAME'] = author
    env['GIT_AUTHOR_EMAIL'] = authorMail
    env['GIT_AUTHOR_DATE'] = authorDate
    env['GIT_COMMITTER_NAME'] = committer
    env['GIT_COMMITTER_EMAIL'] = committerMail
    env['GIT_COMMITTER_DATE'] = committerDate

    command = [b'commit-tree', hexlify(treesha)]
    for parent in parents:
        command.extend([b'-p', hexlify(parent)])
    out = gitwrapper(*command, input=message, env=env)
    return unhexlify(out)

def git_create_tag(gitwrapper, objsha, objtype, name, tagger, taggerMail, date, message):
    env = dict(os.environ)
    env['GIT_COMMITTER_NAME'] = tagger
    env['GIT_COMMITTER_EMAIL'] = taggerMail
    env['GIT_COMMITTER_DATE'] = date
    gitwrapper('tag', name, '-a', '-m', message, hexlify(objsha), env=env)
    out = gitwrapper('show-ref', '--hash', name)
    return unhexlify(out)

def git_cat_file(gitwrapper, sha):
    out = gitwrapper('cat-file', '-p', hexlify(sha), raw=True)
    return out

def pathfile(base, sha):
    sha = hexlify(sha).decode()
    return base.joinpath(sha[:2], sha[2:])

def loose_create_blob(base, content):
    raw, sha = io.loose.object_rawsha(b'blob', content)
    io.loose.object_write(pathfile(base, sha), raw)
    return sha

def loose_create_tree(base, entries):
    content = io.loose.tree_gen(entries)
    raw, sha = io.loose.object_rawsha(b'tree', content)
    io.loose.object_write(pathfile(base, sha), raw)
    return sha

def loose_create_commit(base, treesha, message, author, authorMail, authorDate, committer, committerMail, committerDate, parents=[]):
    author = author+b' <'+authorMail+b'>'
    committer = committer+b' <'+committerMail+b'>'
    content = io.loose.commit_gen(treesha, message, author, bytes(authorDate), committer, bytes(committerDate), parents)
    raw, sha = io.loose.object_rawsha(b'commit', content)
    io.loose.object_write(pathfile(base, sha), raw)
    return sha

def loose_create_tag(base, objsha, objtype, name, tagger, taggerMail, date, message):
    tagger = tagger+b' <'+taggerMail+b'>'
    content = io.loose.tag_gen(objsha, objtype, name, tagger, bytes(date), message)
    raw, sha = io.loose.object_rawsha(b'tag', content)
    io.loose.object_write(pathfile(base, sha), raw)
    return sha

class _sha:
    shas = []
    def __init__(self, index):
        self.index = index

    def _get_sha(self):
        return _sha.shas[self.index]

class now:
    tt = 1422275897
    dt = 0
    def __bytes__(self):
        b = str(now.tt+now.dt).encode() + b' +0000'
        now.dt += 1
        return b


obj_to_create = [ b'blob',   (b'test', ),
                  b'tree',   ([(b'testfile', (b'100644', _sha(-1)))], ),
                  b'commit', (_sha(-1), b"test commit", b"test user", b"test@user.com", now(), b"test committer", b"test@committer", now(),[]),
                  b'commit', (_sha(-2), b"test commit", b"test user", b"test@user.com", now(), b"test committer", b"test@committer", now(), [_sha(-1)]),
                  b'commit', (_sha(-3), b"test commit", b"test user", b"test@user.com", now(), b"test committer", b"test@committer", now(), [_sha(-1), _sha(-2)]),
                  b'tag',    (_sha(-1), b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now(), b"test tag" ),
                  b'tag',    (_sha(-2), b'commit', b'test_tag1', b'test tagger', b'test@tagger.com', now(), b"test tag\n\nOn several lines" ),
                  b'tag',    (_sha(-1), b'tag', b'test_tag2', b'test tagger', b'test@tagger.com', now(), b"tag a tag\n" ),
                ]

git_creators = {b'blob'  : git_create_blob,
                b'tree'  : git_create_tree,
                b'commit': git_create_commit,
                b'tag'   : git_create_tag
               }

loose_creators = {b'blob'  : lambda b, *a : loose_create_blob(b.objectdir,*a),
                  b'tree'  : lambda b, *a : loose_create_tree(b.objectdir,*a),
                  b'commit': lambda b, *a : loose_create_commit(b.objectdir,*a),
                  b'tag'   : lambda b, *a : loose_create_tag(b.objectdir,*a)
                 }

creators = [git_creators, loose_creators]

@pytest.fixture(params=range(int( (1<<(len(obj_to_create)//2)) - 1
                                )
                            )
               )
def combination(request):
    return request.param

def test_equal(tmpdir, combination):
    global ref_contents
    gitwrapper = GitWrapper(tmpdir)
    gitwrapper('init')
    _sha.shas = []
    contents = []
    now.dt = 0
    #import pdb; pdb.set_trace()
    def fix_sha(arg):
        try:
            return arg._get_sha()
        except AttributeError:
            # not a _sha
            try:
                return bytes(arg)
            except TypeError:
                return [fix_sha(a) for a in arg]

    for i in range(0, len(obj_to_create), 2):
        type_, args = obj_to_create[i], obj_to_create[i+1]
        print("-----------------")
        creator = creators[(combination>>(i//2))&1][type_]
        print("-", args)
        _args = [fix_sha(arg) for arg in args]
        print("args ..... ", type_, _args, creator)
        sha = creator(gitwrapper, *_args)
        print("sha..", sha, repr(sha))
        _sha.shas.append(sha)
        with pathfile(gitwrapper.objectdir, sha).open("rb") as f:
            _bytes = f.read()
        contents.append((zlib.decompress(_bytes), _bytes))

    if combination == 0:
        _sha.ref = list(_sha.shas)
        ref_contents = list(contents)
    
    assert ref_contents == contents
    assert _sha.shas == _sha.ref

@pytest.fixture()
def gitwrapper(tmpdir):
    gitwrapper = GitWrapper(tmpdir)
    gitwrapper('init')
    return gitwrapper

def test_maggitw_maggitr_blob1(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(sha) == bytes
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test'

def test_gitw_maggitr_blob1(gitwrapper):
    sha = git_create_blob(gitwrapper, b'test')
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test'

def test_maggitw_gitr_blob1(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test')
    out = git_cat_file(gitwrapper, sha)
    assert out == b'test'

def test_maggitw_maggitr_blob2(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test\n')
    assert type(sha) == bytes
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test\n'

def test_gitw_maggitr_blob2(gitwrapper):
    sha = git_create_blob(gitwrapper, b'test\n')
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test\n'

def test_maggitw_gitr_blob2(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test\n')
    out = git_cat_file(gitwrapper, sha)
    assert out == b'test\n'

def test_maggitw_maggitr_blob3(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test\nOn several lines')
    assert type(sha) == bytes
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test\nOn several lines'

def test_gitw_maggitr_blob3(gitwrapper):
    sha = git_create_blob(gitwrapper, b'test\nOn several lines')
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test\nOn several lines'

def test_maggitw_gitr_blob3(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test\nOn several lines')
    out = git_cat_file(gitwrapper, sha)
    assert out == b'test\nOn several lines'

def test_maggitw_maggitr_blob4(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test\nOn several lines\n')
    assert type(sha) == bytes
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test\nOn several lines\n'

def test_gitw_maggitr_blob4(gitwrapper):
    sha = git_create_blob(gitwrapper, b'test\nOn several lines\n')
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, sha))
    assert type_ == b'blob'
    assert content == b'test\nOn several lines\n'

def test_maggitw_gitr_blob4(gitwrapper):
    sha = loose_create_blob(gitwrapper.objectdir, b'test\nOn several lines\n')
    out = git_cat_file(gitwrapper, sha)
    assert out == b'test\nOn several lines\n'

def test_maggitw_maggitr_dir(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, treesha))
    assert type_ == b'tree'
    assert io.loose.tree_parse(content) == {b'testfile':(b'100644', blobsha)}

def test_gitw_maggitr_dir(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, treesha))
    assert type_ == b'tree'
    assert io.loose.tree_parse(content) == {b'testfile':(b'100644', blobsha)}

def test_maggitw_gitr_dir(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    out = git_cat_file(gitwrapper, treesha)

    assert out == b'100644 blob '+hexlify(blobsha)+b'\ttestfile\n'


def test_maggitw_maggitr_commit(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer", b"now")
    assert type(commitsha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, commitsha))
    assert type_ == b'commit'
    assert io.loose.commit_parse(content) == (treesha, [], [b'test commit'], b'test user <test@user.com> now', b'test committer <test@committer> now')


def test_gitw_maggitr_commit(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    commitsha = git_create_commit(gitwrapper, treesha, b'test commit', b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)
    print("commitshab", commitsha)

    author =  b'test user <test@user.com> ' + now
    committer = b'test committer <test@committer.com> ' + now
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, commitsha))
    assert type_ == b'commit'
    assert io.loose.commit_parse(content) == (treesha, [], [b'test commit'], author, committer)


def test_maggitw_gitr_commit(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    now = bytes(str(time.time()), 'ascii')
    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    out = git_cat_file(gitwrapper, commitsha)

    assert out == b'tree ' + hexlify(treesha) + b'\nauthor test user <test@user.com> ' + now + b'\ncommitter test committer <test@committer.com> ' + now + b'\n\ntest commit'


def test_maggitw_maggitr_1commit(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    commit0sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now")
    assert type(commit0sha) == bytes
    commit1sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit\n", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now", parents=[commit0sha])
    assert type(commit1sha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, commit1sha))
    assert type_ == b'commit'
    assert io.loose.commit_parse(content) == (treesha, [commit0sha], [b'test commit', b''], b'test user <test@user.com> now', b'test committer <test@committer.com> now')


def test_gitw_maggitr_1commit(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    commit0sha = git_create_commit(gitwrapper, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)
    commit1sha = git_create_commit(gitwrapper, treesha, b"test commit\n", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now, [commit0sha])

    author =  b'test user <test@user.com> ' + now
    committer = b'test committer <test@committer.com> ' + now
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, commit1sha))
    assert type_ == b'commit'
    assert io.loose.commit_parse(content) == (treesha, [commit0sha], [b'test commit', b''], author, committer)


def test_maggitw_gitr_1commit(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    now = bytes(str(int(time.time())), 'ascii') + b' +0100'
    commit0sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)
    commit1sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit\n", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now, parents=[commit0sha])

    out = git_cat_file(gitwrapper, commit1sha)

    assert out == b'tree ' + hexlify(treesha) + b'\nparent '+ hexlify(commit0sha) + b'\nauthor test user <test@user.com> ' + now + b'\ncommitter test committer <test@committer.com> ' + now + b'\n\ntest commit\n'

def test_maggitw_maggitr_2commit(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    commit0sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now")
    assert type(commit0sha) == bytes
    commit1sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now", parents=[commit0sha])
    assert type(commit1sha) == bytes
    commit2sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit\nseveral lines", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now", parents=[commit0sha, commit1sha])
    assert type(commit2sha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, commit2sha))
    assert type_ == b'commit'
    assert io.loose.commit_parse(content) == (treesha, [commit0sha, commit1sha], [b'test commit', b'several lines'], b'test user <test@user.com> now', b'test committer <test@committer.com> now')


def test_gitw_maggitr_2commit(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    commit0sha = git_create_commit(gitwrapper, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)
    commit1sha = git_create_commit(gitwrapper, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now, [commit0sha])
    commit2sha = git_create_commit(gitwrapper, treesha, b"test commit\nseveral lines", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now, [commit0sha, commit1sha])

    author =  b'test user <test@user.com> ' + now
    committer = b'test committer <test@committer.com> ' + now
    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, commit2sha))
    assert type_ == b'commit'
    assert io.loose.commit_parse(content) == (treesha, [commit0sha, commit1sha], [b'test commit', b'several lines'], author, committer)


def test_maggitw_gitr_2commit(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    now = bytes(str(int(time.time())), 'ascii') + b' +0100'
    commit0sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)
    commit1sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now, parents=[commit0sha])
    commit2sha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit\nseveral lines" ,b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now, parents=[commit0sha, commit1sha])

    out = git_cat_file(gitwrapper, commit2sha)

    assert out == (b'tree '+hexlify(treesha)
                 + b'\nparent '+hexlify(commit0sha)
                 + b'\nparent '+hexlify(commit1sha)
                 + b'\nauthor test user <test@user.com> '+now
                 + b'\ncommitter test committer <test@committer.com> '+now
                 + b'\n\ntest commit\nseveral lines')

def test_maggitw_maggitr_tag(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now")
    assert type(commitsha) == bytes

    tagsha = loose_create_tag(gitwrapper.objectdir, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', b"now", b"test tag")
    assert type(tagsha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, tagsha))
    assert type_ == b'tag'
    assert io.loose.tag_parse(content) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> now', [b'test tag', b''])


def test_gitw_maggitr_tag(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    commitsha = git_create_commit(gitwrapper, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    tagsha = git_create_tag(gitwrapper, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now, b"test tag")

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, tagsha))
    assert type_ == b'tag'
    assert io.loose.tag_parse(content) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> '+now, [b'test tag', b''])

def test_maggitw_gitr_tag(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    now = bytes(str(int(time.time())), 'ascii')
    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    tagsha = loose_create_tag(gitwrapper.objectdir, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now, b"test tag")

    out = git_cat_file(gitwrapper, tagsha)

    assert out == b'object ' + hexlify(commitsha) + b'\ntype commit\ntag test_tag\ntagger test tagger <test@tagger.com> ' + now + b'\n\ntest tag\n'

def test_maggitw_maggitr_tag2(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now")
    assert type(commitsha) == bytes

    tagsha = loose_create_tag(gitwrapper.objectdir, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', b"now", b"test tag\n")
    assert type(tagsha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, tagsha))
    assert type_ == b'tag'
    assert io.loose.tag_parse(content) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> now', [b'test tag', b''])


def test_gitw_maggitr_tag2(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    commitsha = git_create_commit(gitwrapper, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    tagsha = git_create_tag(gitwrapper, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now, b"test tag\n")

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, tagsha))
    assert type_ == b'tag'
    assert io.loose.tag_parse(content) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> '+now, [b'test tag', b''])

def test_maggitw_gitr_tag2(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    now = bytes(str(int(time.time())), 'ascii')
    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    tagsha = loose_create_tag(gitwrapper.objectdir, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now, b"test tag\n")

    out = git_cat_file(gitwrapper, tagsha)

    assert out == b'object ' + hexlify(commitsha) + b'\ntype commit\ntag test_tag\ntagger test tagger <test@tagger.com> ' + now + b'\n\ntest tag\n'

def test_maggitw_maggitr_tag3(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)
    assert type(treesha) == bytes

    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", b"now", b"test committer", b"test@committer.com", b"now")
    assert type(commitsha) == bytes

    tagsha = loose_create_tag(gitwrapper.objectdir, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', b"now", b"test tag\nseveral lines\n")
    assert type(tagsha) == bytes

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, tagsha))
    assert type_ == b'tag'
    assert io.loose.tag_parse(content) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> now', [b'test tag', b'several lines', b''])


def test_gitw_maggitr_tag3(gitwrapper):
    blobsha = git_create_blob(gitwrapper, b'test')

    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = git_create_tree(gitwrapper, entries)

    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    commitsha = git_create_commit(gitwrapper, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    tagsha = git_create_tag(gitwrapper, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now, b"test tag\nseveral lines\n")

    type_, content = io.loose.object_content(pathfile(gitwrapper.objectdir, tagsha))
    assert type_ == b'tag'
    assert io.loose.tag_parse(content) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> '+now, [b'test tag', b'several lines', b''])

def test_maggitw_gitr_tag3(gitwrapper):
    blobsha = loose_create_blob(gitwrapper.objectdir, b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = loose_create_tree(gitwrapper.objectdir, entries)

    now = bytes(str(int(time.time())), 'ascii')
    commitsha = loose_create_commit(gitwrapper.objectdir, treesha, b"test commit", b"test user", b"test@user.com", now, b"test committer", b"test@committer.com", now)

    tagsha = loose_create_tag(gitwrapper.objectdir, commitsha, b'commit', b'test_tag', b'test tagger', b'test@tagger.com', now, b"test tag\nseveral lines\n")

    out = git_cat_file(gitwrapper, tagsha)

    assert out == b'object ' + hexlify(commitsha) + b'\ntype commit\ntag test_tag\ntagger test tagger <test@tagger.com> ' + now + b'\n\ntest tag\nseveral lines\n'

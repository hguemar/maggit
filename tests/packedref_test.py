# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest
from maggit.db.io.packedref import _packed_ref_parse_lines, packed_ref_parse
from binascii import unhexlify
import os


samples_empty = b''

def test_empty():
    refs = _packed_ref_parse_lines(samples_empty.split(b'\n'))
    assert refs == []

samples_empty_header1 = b'''# pack-refs with: peeled fully-peeled'''
def test_empty_header1():
    refs = _packed_ref_parse_lines(samples_empty_header1.split(b'\n'))
    assert refs == []

samples_empty_header2 = b'''# pack-refs with: peeled fully-peeled\n'''
def test_empty_header2():
    refs = _packed_ref_parse_lines(samples_empty_header2.split(b'\n'))
    assert refs == []


samples_simple_ref = b'''# pack-refs with: peeled fully-peeled
f34be46e47773d03e9d09641209121591a6b37c8 refs/heads/master
f34be46e47773d03e9d09641209121591a6b37c8 refs/remotes/origin/master'''
def test_simple_ref():
    refs = _packed_ref_parse_lines(samples_simple_ref.split(b'\n'))
    assert refs == [(b'refs/heads/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None),
                    (b'refs/remotes/origin/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None)
                   ]

samples_simple_ref_empty_line = b'''# pack-refs with: peeled fully-peeled
f34be46e47773d03e9d09641209121591a6b37c8 refs/heads/master

f34be46e47773d03e9d09641209121591a6b37c8 refs/remotes/origin/master
'''
def test_simple_ref_empty_line():
    refs = _packed_ref_parse_lines(samples_simple_ref_empty_line.split(b'\n'))
    assert refs == [(b'refs/heads/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None),
                    (b'refs/remotes/origin/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None)
                   ]

samples_peeled_ref = b'''# pack-refs with: peeled fully-peeled
f34be46e47773d03e9d09641209121591a6b37c8 refs/heads/master
f34be46e47773d03e9d09641209121591a6b37c8 refs/remotes/origin/master
d5aef6e4d58cfe1549adef5b436f3ace984e8c86 refs/tags/tag-v1
^3d654be48f65545c4d3e35f5d3bbed5489820930
33682a5e98adfd8ba4ce0e21363c443bd273eb77 refs/tags/tag-v2
^7214aea37915ee2c4f6369eb9dea520aec7d855b'''

def test_peeled_ref():
    refs = _packed_ref_parse_lines(samples_peeled_ref.split(b'\n'))
    assert refs == [(b'refs/heads/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None),
                    (b'refs/remotes/origin/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None),
                    (b'refs/tags/tag-v1', unhexlify(b'd5aef6e4d58cfe1549adef5b436f3ace984e8c86'), unhexlify(b'3d654be48f65545c4d3e35f5d3bbed5489820930')),
                    (b'refs/tags/tag-v2', unhexlify(b'33682a5e98adfd8ba4ce0e21363c443bd273eb77'), unhexlify(b'7214aea37915ee2c4f6369eb9dea520aec7d855b'))
                   ]

def test_peeled_ref_file(tmpdir):
    filename = os.path.join(str(tmpdir), 'test')
    with open(filename, mode='wb') as f:
        f.write(samples_peeled_ref)

    refs = packed_ref_parse(filename)
    assert refs == [(b'refs/heads/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None),
                    (b'refs/remotes/origin/master', unhexlify(b'f34be46e47773d03e9d09641209121591a6b37c8'), None),
                    (b'refs/tags/tag-v1', unhexlify(b'd5aef6e4d58cfe1549adef5b436f3ace984e8c86'), unhexlify(b'3d654be48f65545c4d3e35f5d3bbed5489820930')),
                    (b'refs/tags/tag-v2', unhexlify(b'33682a5e98adfd8ba4ce0e21363c443bd273eb77'), unhexlify(b'7214aea37915ee2c4f6369eb9dea520aec7d855b'))
                   ]

